"""Collection of functionality to analyze the vapor film experiments.

Currently, this contains mostly 2D geometry stuff for video analysis.
"""
import numpy as np
from numpy.typing import NDArray, ArrayLike
import pandas as pd
from pathlib import Path
from typing import Union

from twodpg import dist_avg, avg_dist, centroid, pol_lengths, points_in_polygon
from coordspicker import load_rmjson, save_rmjson


def shift_vertices(pol: NDArray) -> NDArray:
    """Shift polygon elements so that they start with the vertex that has the
    largest distannce to the last vertex.
    
    Parameters
    ----------
    pol : ndarray
        A shape (N, 2) array.
    """
    diff = pol - np.roll(pol, 1, axis=0)
    idx = np.argmax(diff[:, 0] ** 2 + diff[:, 1] ** 2)
    return np.roll(pol, pol.shape[0] - idx, axis=0)


def interpolate(p1: ArrayLike, p2: ArrayLike,
                val: float, coord: str = 'y') -> NDArray:
    """Interpolate linearly between `p1` and `p2` using one coordinate `val`.
    
    Given two points `p1`, `p2` and a target coordinate, which is assumed to
    lie on a line between the two points, this computes the other coordinate
    value assuming a straight line connection.
    
    Parameters
    ----------
    p1
    p2 : array_like
        Length 2 input points.
    val : float
        Target coordinate's value.
    coord : str, default='y'
        As which coordinate to interpret `val`.
    
    Returns
    -------
    NDArray
        
    """
    assert not np.all(p1 == p2), \
        f"Can only interpolate between different points, but got same p={p1}."
    if coord == 'y':
        if p1[1] != p2[1]:
            i1, i2 = 1, 0
        else:
            print(f'interpolate: Found identical y-coordinates (y={p1[1]}). '
                  f'switching to x-interpolation.')
            i1, i2 = 0, 1
    elif coord == 'x':
        if p1[0] != p2[0]:
            i1, i2 = 0, 1
        else:
            print(f'interpolate: Found identical x-coordinates (x={p1[0]}). '
                  f'switching to y-interpolation.')
            i1, i2 = 1, 0
    else:
        raise ValueError(
            f'`coord` needs to be one of "x", "y", but I got {coord=}.')
    u = (val - p1[i1]) / (p2[i1] - p1[i1])
    ret = np.array([np.nan, np.nan])
    ret[[i1, i2]] = val, p1[i2] + u * (p2[i2] - p1[i2])
    return ret


def split_left_right(pol: NDArray, ref: ArrayLike) -> tuple:
    """Split polygon into left and right sides of reference ref.
    """
    left = pol[pol[:, 0] <= ref[0]]
    right = pol[pol[:, 0] >= ref[0]]
    return left, right


def segments(pol: NDArray, ymin: ArrayLike, ymax: ArrayLike,
             ref: ArrayLike) -> dict:
    """Return segments of input polygon `pol` that split it at given ymin,
    ymax values. To distinguish between left and right side of the sample
    `pol` is split relative to `ref` (typically the centroid of `pol`).
    
    Parameters
    ----------
    pol : NDArray
        Shape (N, 2) array of the polygon's vertices.
    ymin : array_like
        Minimum values of y bands. Must be same length as `ymax`.
    ymax : array_like
        Maximum values of y bands. Must be same length as `ymin`.
    ref : array_like
        Vertex of the reference point.
    
    Returns
    -------
    dict
        Returns a dictionary of polygon segments, grouped into keys 'left',
        'right', 'center'. The segments are open polygons that start and end
        exactly at ymin, ymax, and the corresponing x values. Verteces within
        the bands are not touched. The 'center' element contains only one
        segment, which contains the sample's bottom (max. ymax).
    """
    ymin, ymax = np.asarray(ymin), np.asarray(ymax)
    ymin = np.array([ymin]) if ymin.ndim < 1 else ymin
    ymax = np.array([ymax]) if ymax.ndim < 1 else ymax
    assert np.all(ymin < ymax), \
        f'Something is wrong about the segment boundaries.\n'\
        f'Got {ymin=}, {ymax=}.\n'\
        f'Segments need to increase in y (top->bottom). Is that the case?'
    left, right = split_left_right(pol, ref)
    mx = ymax.max()
    ret = {'left': [], 'right': [], 'center': []}
    for i, (ymi, yma) in enumerate(zip(ymin, ymax)):
        if yma == mx:
            k = 0
            while pol[k, 1] < ymi:
                k += 1
            idxs = k - 1 if k > 0 else 0
            k += 1
            while pol[k, 1] > ymi:
                k += 1
            idxe = k + 1
            sel = pol[idxs:idxe].copy()
            sel[0] = interpolate(sel[0], sel[1], ymi)
            sel[-1] = interpolate(sel[-2], sel[-1], ymi)
            ret['center'] = sel
        else:
            k = 0
            while left[k, 1] < ymi:
                k += 1
            idxmi = k - 1 if k > 0 else 0
            while left[k, 1] < yma:
                k += 1
            idxma = k + 1
            sel = left[idxmi:idxma].copy()
            sel[0] = interpolate(sel[0], sel[1], ymi)
            try:
                sel[-1] = interpolate(sel[-2], sel[-1], yma)
            except AssertionError as err:
                print(f'{sel=},\n{k=},\n{left=},\n{idxmi=},\n{idxma=},\n'
                      f'{left[idxmi:idxma]=},\n{ymi=},\n {yma=},\n{ret=}')
                raise err
            ret['left'].append(sel)
            k = 0
            while right[k, 1] > yma:
                k += 1
            idxma = k - 1
            while right[k, 1] > ymi:
                k += 1
            idxmi = k + 1
            sel = right[idxma:idxmi].copy()
            sel[0] = interpolate(sel[0], sel[1], yma)
            start = 1 if np.all(sel[0] == sel[1]) else 0
            sel[-1] = interpolate(sel[-2], sel[-1], ymi)
            end = -1 if np.all(sel[-2] == sel[-1]) else None
            ret['right'].append(sel[start:end])
    return ret


def moving_avg(x: ArrayLike, width: int = 3) -> NDArray:
    """Computes a moving average of input `x` and window of `width` samples.
    
    The window is centered around the running index. To make this precise,
    `width` is increased by 1 if an even value is given. At the start and end of
    `x` the window width is decreased, so that the first and last elements
    returned always match the first and last elements of input `x`.
    
    Parameters
    ----------
    x : ArrayLike
        Input values to average.
    width: int
        Window width.
    """
    x = np.asarray(x)
    if width % 2 == 0:
        width += 1
    lnh = (width - 1) // 2
    ret = np.empty_like(x)
    idx = (
        np.arange(lnh, x.shape[0] - lnh)[:, None] * np.ones(width, dtype=int)
    ) + np.arange(width) - lnh
    ret[lnh:-lnh] = np.sum(x[idx], axis=1)
    ret[lnh:-lnh] /= width
    ret[:lnh] = np.cumsum(x[:2 * lnh])[::2] / (2 * np.arange(lnh) + 1)
    ret[-lnh:] = np.cumsum(x[-2 * lnh:][::-1])[::-2] \
        / ((2 * np.arange(lnh) + 1)[::-1])
    ret[-1] = x[-1]  # Don't understand why above does not work for last element
    return ret


def bands(pol, nbands: int = 10) -> NDArray:
    """Returns `nbands` + 1 y-coordinates of boundaries matching a polygon
    assumed to be the sample outline.
    
    Parameters
    ----------
    pol : NDarray
        Sample outline.
    nbands : int
        Number of ybands to return.
    """
    top = max(pol[0, 1], pol[-1, 1])
    btm = pol[:, 1].max()
    return np.linspace(btm, top, nbands + 1, endpoint=True)


def delta_f(outer: NDArray, inner: NDArray, n_bands: int) -> dict:
    """Compute average film thicknesses for a number of y-regions ('bands').
    
    Parameters
    ----------
    outer : NDArray
        Outer polygon Nx2 array.
    inner : NDArray
        Inner polygon Nx2 array.
    n_bands : int
        Number of y-regions to create. They will be spaced evenly over the
        height of the sample.
    
    Returns
    -------
    Dictionary with keys 'data', 'bandwidth', 'centroid'. 'bandwidth' is the
    thickness of each y-band. 'data' is a pandas dataframe with (absolute)
    y-coordinates of the bands and the corresponding film thickness values.
    """
    ybands = bands(inner)[::-1]
    bdu = ybands[:-1]
    bdl = ybands[1:].copy()
    cntr = .5 * (centroid(outer) + centroid(inner))
    sgmi = segments(inner, ymin=bdu, ymax=bdl, ref=cntr)
    bdl[-1] = outer[:, 1].max()
    # print(f"delta_f: {bdu=}, {bdl=}")
    sgmo = segments(outer, ymin=bdu, ymax=bdl, ref=cntr)
    film = pd.DataFrame(
        index=pd.RangeIndex(n_bands, name='band'), columns=['y', 'delta'])
    for i, sgol, sgor, sgil, sgir, bu, bl in zip(
        range(n_bands), sgmo['left'], sgmo['right'],
        sgmi['left'], sgmi['right'], bdu, bdl
    ):
        delta = .5 * (dist_avg(sgol, sgil) + dist_avg(sgor, sgir))
        film.loc[i, :] = .5 * (bu + bl), delta
    delta = dist_avg(sgmo['center'], sgmi['center'])
    film.loc[n_bands - 1, :] = .5 * (bdu[-1] + bdl[-1]), delta
    bw = (bdu[-1] - bdu[0]) / (n_bands - 1)
    # film_exists = film.loc[film['delta'] > 0.5, 'delta']
    return {
        'film': film, 'bandwidth': bw, 'centroid': cntr,
        'segments': {'outer': sgmo, 'inner': sgmi}
    }


def mean_thickn(frame: dict, nys: int, write_to_frame: bool = False,
                thr: float = 0.75) -> tuple:
    """Compute the mean film thickness of a given frame.
    
    Parameters
    ----------
    frame : dict
        Film thickness and other data created by `delta_f()`.
    
    Returns
    -------
        tuple
    Three element tuple:
        (mean thickness,
         length of film covered surface,
         length of direct contact surface)
    """
    delt_mean = 0.
    length = 0
    length_dc = 0.
    film = frame['film']
    segs = frame['segments']['inner']
    for band, el in film.iterrows():
        delta = el['delta']
        if band < nys - 1:
            l = pol_lengths(segs['left'][band]).sum() \
                    + pol_lengths(segs['right'][band]).sum()
            if delta > thr:
                delt_mean += delta * l
                length += l
            else:
                length_dc += l
        else:
            pol_lengths(segs['center']).sum()
            if delta > thr:
                delt_mean += delta * l
                length += l
            else:
                length_dc += l
    delt_mean /= length
    if write_to_frame:
        frame['delta_mean'] = delt_mean
        frame['length_film'] = length
        frame['length_direct'] = length_dc
    return delt_mean, length, length_dc


def radii(refshape: NDArray, filmshape: NDArray) -> tuple:
    """Compute the two centroids and average radii.
    
    Returns:
    --------
        ref_r, flm_r, ref_cntr, flm_cntr
    """
    ref_cntr = centroid(refshape)
    flm_cntr = centroid(filmshape)
    ref_r = avg_dist(refshape, ref_cntr, -1, False)
    flm_r = avg_dist(filmshape, flm_cntr, -1, False)
    return ref_r, flm_r, ref_cntr, flm_cntr


def direct_area(ref, directs, verts, ysurf, nbands: int = 10) -> dict:
    """Compute per y-band direct contact areas from outline polygons.
    
    Steps done:
    - Check each pixel location, if it lies inside, outside or on the edge of
      the reference polygon. For each y-band Count the number of pixels in
      that band.
    - For every of the direct contact polygons, check whether each pixel
      location lies inside the polygon.
    - For every vertical band count the pixels which of the pixels lying inside
      each polygon also have y-coordinates that belong to the band.
    
    Parameters
    ----------
    ref : ndarray
        Sample outline
    directs : array_like
        List of outlines of direct contact areas.
    verts : NDArray
        Raster's vertices.
    ysurf : int
        Vertical location of the water surface.
    nbands : int
        Number of vertical bands to use.
    
    Returns
    -------
    Dictionary with count of pixels in each y-band:
    - `'ref'`: inside the reference polygon
    - `'direct'`: inside the direct contact polygons
    Each pixel location corresponds to an area of one square pixel.
    """
    dcont_area = np.zeros(nbands)
    ref_area = np.nan * np.zeros(nbands)
    rk = points_in_polygon(verts, ref) > 0
    dks = [points_in_polygon(verts, dpol) > 0 for dpol in directs]
    ybands = bands(ref)
    for i in range(nbands - 1):
        lower, upper = ybands[i], ybands[i + 1]
        if lower < ysurf:
            break
        if upper < ysurf:
            upper = ysurf
        iyband = np.logical_and(verts[:, 1] <= lower, verts[:, 1] > upper)
        ref_area[i] = np.count_nonzero(np.logical_and(iyband, rk))
        for dk in dks:
            dcont_area[i] += np.count_nonzero(np.logical_and(iyband, dk))
    return {'ref': ref_area, 'direct': dcont_area}


class ItemNotFoundError(Exception):
    pass


class Rois:
    """Base skeleton to load rois from json dump.
    """
    len_key = 'number of items'

    def __init__(self, pth=None):
        """
        Parameters
        ----------
        pth : str or Path
            File name to load rois from.
        """
        self.pth = Path(pth) if pth is not None else None
        self.content = {}
        if self.pth is not None:
            self.content = load_rmjson(pth)

    def __getitem__(self, key):
        if key in self.content:
            return self.content[key]
        else:
            raise KeyError(f'No key {key} found in rois.')

    def __len__(self):
        if self.len_key in self.content.keys():
            return self.content[self.len_key]
        else:
            return 0


class FilmPolygons(Rois):
    """Access polygons of the VFE experiments on a per-frame basis.
    
    This object implements a key name convention to access polygon coordinates
    of a roimanager JSON dump. The name convention is:  
        `[prefix]_[fnum]` or `direct_[fnum]-[n]`
    - `[prefix]: One of 'ref', 'film', 'direct', identifying a polygon as
      one of sample outline, vapor film boundary or direct contact area.
    - `[fnum]`: An integer representing the video frame.
    - `[n]`: An integer uniquely identifying one of possibly several direct
      contact outlines.
    
    Parameters
    ----------
    pth : str or pathlib.Path
        File name to load rois from.
    sort_content_pols : bool
        Whether or not to sort the list of polygons by frame number. This will
        sort polygons by frame, then ref before film before directs.
    overwrite_with_sorted : bool
        Whether to overwrite the original source JSON. If false, content will
        be stored in `pth`_sorted.json.
    
    Attributes
    ----------
    frames : NDarray
        The frame numbers as array of integers.
    fkeys : dict
        This is a dictionary that holds all polygons and may be accessed
        directly. It is, however more convenient to use the bracketed [framenum]
        (__getitem__) for that, which dynamically return ndarrays.
    """
    frame_prefs: list = ["ref", "film", "direct"]

    def __init__(
        self, pth=None, sort_content_pols: bool = False,
        overwrite_with_sorted: bool = False, frame_offset: int = 1
    ):
        Rois.__init__(self, pth)
        self.frame_offset: int = frame_offset
        self.fkeys: dict = {}
        self._get_frames()
        self._sort_factor: int = self._get_sort_factor()
        if sort_content_pols:
            self.sort_content_polygons()
            p = self.pth
            if not overwrite_with_sorted:
                self.pth: Path = p.parent / Path(f"{p.stem}_sorted{p.suffix}")
            self.save_to_rmjson(self.pth)

    def __len__(self):
        return len(self.frames)
    
    def __getitem__(self, key) -> tuple:
        ret = []
        fk = self.fkeys
        if isinstance(key, tuple):
            fnum = key[0] + self.frame_offset
            keys = key[1:]
            if keys[0] in ('direct', 'directs'):
                keys = [k for k in fk[fnum].keys() if 'direct' in k]
                keys.sort()
        else:
            fnum = key + self.frame_offset
            keys = list(fk[fnum].keys())
            ret.append(tuple(keys))
        fkel = fk[fnum]
        for k in keys:
            if k not in fkel.keys():
                raise KeyError(f"No key '{k}' in frame {fnum}. "
                               f"Available keys: {list(fkel.keys())}.")
            try:
                v = np.asarray(self.content['polygons'][fkel[k]]['xy'], dtype=float)
            except IndexError as err:
                print(f'{k=}, {fkel[k]=}, {key=}')
                raise err
            if k in ('ref', 'film'):
                v = shift_vertices(v)
            ret.append(v)
        return tuple(ret)

    def _get_frames(self):
        self.fkeys = {}
        fp, fk = self.frame_prefs, self.fkeys
        for i, pol in enumerate(self.content['polygons']):
            polnum = -1
            try:
                pref, num = pol['name'].split("_")
            except ValueError:
                print(f"Found a wrongly formatted key: '{pol['name']}'.")
                continue
            if '-' in num:
                num, polnum = num.split("-")
                polnum = int(polnum)
            num = int(num)
            if pref in fp:
                if num not in fk.keys():
                    fk[num] = {}
                if pref == 'direct':
                    pref = f'direct-{polnum}'
                fk[num][pref] = i
    
    def _set_frame_names(self, frames: ArrayLike):
        pols = []
        old_pols = self.content['polygons']
        old_frames = self.frames.copy()
        for el in old_pols:
            try:
                pref, num = el['name'].split('_')
            except ValueError:
                print(f'''Found invalid key format: "{el['name']}".''')
                continue
            if "-" in num:
                num, dnum = num.split("-")
                dnum = int(dnum)
                name_end = f"-{dnum}"
            else:
                name_end = ""
            num = int(num) - self.frame_offset
            try:
                fnum = frames[np.where(old_frames == num)[0][0]]
            except IndexError as err:
                print(f'{pref=}, {num=}, {name_end=}')
                raise err
            dct = dict(
                name=f'{pref}_{fnum + self.frame_offset}{name_end}',
                length=el['length'], type=el['type'], xy=el['xy']
            )
            pols.append(dct)
        self.content['polygons'] = pols
        self._get_frames()
    
    @property
    def frames(self) -> NDArray:
        ret = np.asarray(list(self.fkeys.keys())) - self.frame_offset
        ret.sort()
        return ret
    
    @frames.setter
    def frames(self, frames: NDArray | list):
        frames = frames.copy()
        frames.sort()
        self._set_frame_names(frames)

    def get_polies(self, fnum: int) -> tuple:
        """
        Returns
        -------
        (refshape, flmshape)
        """
        nmr, nmf, foundr, foundf = f'ref_{fnum}', f'film_{fnum}', False, False
        for pol in self.content['polygons']:
            if not foundr:
                if nmr == pol['name']:
                    refshape = np.asarray(pol['xy'], dtype=float)
                    foundr = True
            if not foundf:
                if nmf == pol['name']:
                    filmshp = np.asarray(pol['xy'], dtype=float)
                    foundf = True
            if foundr and foundf:
                return shift_vertices(refshape), shift_vertices(filmshp)
        raise ItemNotFoundError(f"Could not find frame {fnum}.")
    
    def _get_sort_factor(self) -> int:
        # get max. number of polygons in one frame
        mx = 0
        for f in self.frames:
            try:
                ln = len(self[f][0])
            except IndexError as err:
                print(f'frame: {f}, {mx=}')
                raise err
            mx = ln if ln > mx else mx
        return 10 ** (int(np.log(mx)) + 1)
    
    def sortkey_ref_film_direct(self, k: dict) -> int:
        """Returns an integer based on frame number 
        """
        fac = self._sort_factor
        one, two = k['name'].split('_')
        three = two.split('-')
        if one == 'ref':
            ret = fac * int(three[0])
        elif one == 'film':
            ret = fac * int(three[0]) + 1
        elif one == 'direct':
            ret = fac * int(three[0]) + 1 + int(three[1])
        else:
            print(f'''Wrong format of key "{k['name']}"''')
            ret = -1
        return ret
    
    def sort_content_polygons(self):
        self.content['polygons'].sort(key=self.sortkey_ref_film_direct)
        self._get_frames()
    
    def save_to_rmjson(self, pth: Union[str, Path]):
        save_rmjson(self.content, pth)
