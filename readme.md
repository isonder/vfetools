# `vfetools`

Collection of functionality to analyze the vapor film experiments.

Currently, this contains mostly 2D geometry stuff for video analysis.

## Install

This is meant to run in a virtual environment. The module depends on `numpy`,
`pandas` and , my [`twodpg`](https://gitlab.com/isonder/twodpg) and
[`coordspicker`](https://gitlab.com/isonder/coordspicker) packages. Please
install before this. Then clone the code, change into the new `vfetools` folder
and install:
```shell
git clone https://gitlab.com/isonder/vfetools.git
cd vfetools
pip install -e .
```

Use at your own risk!


## Functionality

[WORK IN PROGRESS]

### Segmenting

#### `bands()`
Computes $`y`$-coordinates for a given number of boundaries and one a polygon,
so that the maximum and minimum match the polygon's vertical extent.

#### `segments()`
The `segments` function cuts a polygon at given vertical coordinates. Start and
end points are created at the intersection of the polygon and the specified
boundaries. This works hand in hand with the `bands` function.


### Distances

#### `delta_f()`

This computes the average distance ('delta') betweed two not intersecting curves
(polygons) $`P`$ and $`S`$, which may be closed or open (typically they are open).
The average distance is calculated according to the line integral

```math
\langle\delta\rangle =
\frac{1}{L} \int_{\text{line}} \delta_{S,\text{min}}({\boldsymbol r})\, dl \quad.
```

Here, $`\boldsymbol{r}=(x, y)`$ is the position vector for an arbitrary point on
polygon $`P`$, $`L`$ is the length of the integrated line, and $`\delta_{S,\text{min}}`$ is
the minimum distance of point $`{\boldsymbol r}`$ to any point on $`S`$.

### Area

#### `direct_area()`

Useful to compute the direct contact area. The fuction determins whether
a pixel lies inside or outside a given set of (closed) polygons, and counts the
ones inside.

### Coordinates, Formats \& I/O

#### `FilmPolygons`

This is a container that can be used to quickly access the polygon coordinates 
stored in the JSON files, created by the "ROIManager dump/load" plugins.
After creating an instance, polygons can be accessed by frame number:
```python
pols = FilmPolygons(
    'path/to/file.json', sort_content_pols=False, overwrite_with_sorted=False,
    frame_offset=1
)
```

##### Itemized Access
```python
fnum = 50
pols[fnum]  # returns a tuple that contains all polygons. The first element is
            # a list of labels that describe the polygons.
```
Specific polygons can be queried by frame number and key word:
```python
reference, = pols[fnum, 'ref']
film, = pols[fnum, 'film']
directs = pols[fnum, 'direct']
```
The return value is always a tuple: `'ref'` and `'film'` keyword statements
return a length 1 tuple, not directly the coordinate array...